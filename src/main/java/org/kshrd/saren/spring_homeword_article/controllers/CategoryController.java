package org.kshrd.saren.spring_homeword_article.controllers;

import org.kshrd.saren.spring_homeword_article.models.Category;
import org.kshrd.saren.spring_homeword_article.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class CategoryController {
    private CategoryService categoryService;
    @Autowired
    CategoryController(CategoryService categoryService){
        this.categoryService=categoryService;
    }
    @GetMapping("admin/list_category")
    public String listArticle(ModelMap model){
       model.addAttribute("category",categoryService.findCategory()) ;
       return "admin/category/list";
    }
    @GetMapping("admin/update_category/{id}")
    public String updateToForm(@PathVariable("id") int id,ModelMap m){
        m.addAttribute("category",categoryService.finOne(id)) ;
        return "admin/category/update";
    }
    @PostMapping("admin/update_category/submit")
    public String update(Category category){
        categoryService.update(category);
        return "redirect:/admin/list_category";
    }
    @GetMapping("admin/delete_category/{id}")
    public String delete(@PathVariable int id){
        categoryService.delete(id);
        return "redirect:/admin/list_category";
    }

    @GetMapping("admin/add_category")
    public String add(ModelMap model,Category category){
        model.addAttribute("category",category) ;
        return "admin/category/add";
    }

    @PostMapping("admin/save_category")
    public String save(@Valid @ModelAttribute Category category, BindingResult result, ModelMap model){
        if(result.hasErrors()){
            model.addAttribute("category",category) ;
            return "admin/category/add";
        }
        categoryService.save(category);
        return "redirect:/admin/list_category";


    }
}
