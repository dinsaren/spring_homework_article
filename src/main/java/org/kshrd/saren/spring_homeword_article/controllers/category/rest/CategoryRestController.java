package org.kshrd.saren.spring_homeword_article.controllers.category.rest;

import org.kshrd.saren.spring_homeword_article.models.Category;
import org.kshrd.saren.spring_homeword_article.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/kvt/api/categories")
public class CategoryRestController {
    Map<String,Object> map=new HashMap<>();
    HttpStatus httpStatus=null;
    private CategoryService categoryService;
    @Autowired
    CategoryRestController(CategoryService categoryService){
        this.categoryService=categoryService;
    }
    @GetMapping
    public ResponseEntity<Map<String,Object>>findAllCategtory(){
        List<Category> categories=categoryService.findCategory();
        try{
            if(!categories.isEmpty()){
                httpStatus=HttpStatus.OK;
                map.put("code","200");
                map.put("message","Successfull");
                map.put("status",true);
                map.put("data",categories);
            }else{
                httpStatus=HttpStatus.NOT_FOUND;
                map.put("code","404");
                map.put("message","fail");
                map.put("status",false);
            }
        }catch (Exception e){
                e.printStackTrace();
                map.put("code","500");
                map.put("message","Something is broken hard");
                map.put("status",false);
                httpStatus=HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<Map<String,Object>> (map,httpStatus=HttpStatus.OK);
    }
    //Add New Article API
    @PostMapping("/add")
    public ResponseEntity<Map<String, Object>> save(@RequestBody Category category){
        try{
            if(categoryService.save(category)){
                httpStatus=HttpStatus.OK;
                map.put("code","200");
                map.put("message","Category has been inserted");
                map.put("status",true);
            }else{
                httpStatus=HttpStatus.NOT_FOUND;
                map.put("code","404");
                map.put("message","fail");
                map.put("status",false);
            }
        }catch (Exception e){
            e.printStackTrace();
            map.put("code","500");
            map.put("message","Something is broken hard");
            map.put("status",false);
            httpStatus=HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(map,httpStatus=HttpStatus.OK);
    }
    //delete category by uuid
    @GetMapping("/id/{id}")
    public ResponseEntity<Map<String, Object>> findCategoryOne(@PathVariable("id") Integer id){
        Category category=categoryService.finOne(id);
        try{
            if(category!=null){
                httpStatus=HttpStatus.OK;
                map.put("code","200");
                map.put("message","Successfull");
                map.put("status",category);
                System.out.println(category);
            }else{
                httpStatus=HttpStatus.NOT_FOUND;
                map.put("code","404");
                map.put("message","fail");
                map.put("status",false);
            }
        }catch (Exception e){
            e.printStackTrace();
            map.put("code","500");
            map.put("message","Something is broken hard");
            map.put("status",false);
            httpStatus=HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(map,httpStatus=HttpStatus.OK);
    }
    @PostMapping("/delete/{id}")
    public ResponseEntity<Map<String, Object>> delete(@PathVariable("id") Integer id){
        Boolean delete=categoryService.delete(id);
        try{
            if(delete){
                httpStatus=HttpStatus.OK;
                map.put("code","200");
                map.put("message","Successfull");
                map.put("status",true);
            }else{
                httpStatus=HttpStatus.NOT_FOUND;
                map.put("code","404");
                map.put("message","fail");
                map.put("status",false);
            }
        }catch (Exception e){
            e.printStackTrace();
            map.put("code","500");
            map.put("message","Something is broken hard");
            map.put("status",false);
            httpStatus=HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(map,httpStatus=HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity<Map<String, Object>> update(@RequestBody Category category){
        try{
            if(categoryService.update(category)){
                httpStatus=HttpStatus.OK;
                map.put("code","200");
                map.put("message","Category has been inserted");
                map.put("status",true);
            }else{
                httpStatus=HttpStatus.NOT_FOUND;
                map.put("code","404");
                map.put("message","fail");
                map.put("status",false);
            }
        }catch (Exception e){
            e.printStackTrace();
            map.put("code","500");
            map.put("message","Something is broken hard");
            map.put("status",false);
            httpStatus=HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(map,httpStatus=HttpStatus.OK);
    }


}
