package org.kshrd.saren.spring_homeword_article.models;

public class Gallary {
    private int id;
    private Article article;
    private String src;

    public Gallary(){

    }

    public Gallary(int id, Article article, String src) {
        this.id = id;
        this.article = article;
        this.src = src;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }
}
