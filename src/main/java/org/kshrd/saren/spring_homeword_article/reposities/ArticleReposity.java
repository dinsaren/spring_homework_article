package org.kshrd.saren.spring_homeword_article.reposities;

import org.apache.ibatis.annotations.*;
import org.kshrd.saren.spring_homeword_article.models.Article;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ArticleReposity {
    //Select Article
    @Select("select a.id,a.title,a.description,a.thumbnail,a.article_hash,a.category_id,a.author,c.name,a.import_date from tbl_articles as a inner join tbl_categories as c on c.id=a.category_id")
   @Results({
           @Result(property = "id",column="id"),
           @Result(property = "title",column="title"),
           @Result(property = "desc",column="description"),
           @Result(property = "thumbnail",column="thumbnail"),
           @Result(property = "articleHash",column="article_hash"),
           @Result(property = "category.id",column="category_id"),
           @Result(property = "importDate",column="import_date"),
           @Result(property = "category.name",column="name"),
           @Result(property = "author",column="author")
   })
    public List<Article>findAllArticle();
    //Insert Article
    @Insert("insert into tbl_articles(title,description,thumbnail,article_hash,category_id,import_date,author) " +
            "VALUES(#{article.title}," +
            "#{article.desc}," +
            "#{article.thumbnail}," +
            "#{article.articleHash}," +
            "#{article.category.id}," +
            "#{article.importDate}," +
            "#{article.author})")
    @Results({
            @Result(property = "title",column="title"),
            @Result(property = "desc",column="description"),
            @Result(property = "thumbnail",column="thumbnail"),
            @Result(property = "articleHash",column="article_hash"),
            @Result(property = "category.id",column="category_id"),
            @Result(property = "importDate",column="import_date"),
            @Result(property = "author",column="author")
    })
    public boolean saveArticle(@Param ("article") Article article);
    //Find By user hash
    @Select("select " +
            "a.id," +
            "a.title," +
            "a.description," +
            "a.thumbnail," +
            "a.article_hash," +
            "a.category_id," +
            "a.author," +
            "c.name,a." +
            "import_date" +
            " from tbl_articles as a " +
            "inner join tbl_categories as c on" +
            " c.id=a.category_id WHERE a.article_hash=#{articleHash} ")
    @Results({
            @Result(property = "id",column="id"),
            @Result(property = "title",column="title"),
            @Result(property = "desc",column="description"),
            @Result(property = "thumbnail",column="thumbnail"),
            @Result(property = "articleHash",column="article_hash"),
            @Result(property = "category.id",column="category_id"),
            @Result(property = "importDate",column="import_date"),
            @Result(property = "category.name",column="name"),
            @Result(property = "author",column="author")
    })
    public Article findArticleOne(String articleHash);

    //
    @Delete("delete from tbl_articles WHERE article_hash=#{articleHash}")
    public boolean delete(String articleHash);
    @Update("update tbl_articles set title=#{title},description=#{desc},category_id=#{category.id},thumbnail=#{thumbnail},author=#{author} WHERE article_hash=#{articleHash}")
    @Results({
            @Result(property = "id",column="id"),
            @Result(property = "title",column="title"),
            @Result(property = "desc",column="description"),
            @Result(property = "thumbnail",column="thumbnail"),
            @Result(property = "articleHash",column="article_hash"),
            @Result(property = "category.id",column="category_id"),
            @Result(property = "author",column="author")
    })
    public boolean update(Article article);
}
