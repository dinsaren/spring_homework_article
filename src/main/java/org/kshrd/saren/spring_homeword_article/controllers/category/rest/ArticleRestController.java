package org.kshrd.saren.spring_homeword_article.controllers.category.rest;

import org.kshrd.saren.spring_homeword_article.models.Article;
import org.kshrd.saren.spring_homeword_article.services.ArticleService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController

@RequestMapping("/kvt/api/articles")
public class ArticleRestController {
    HttpStatus httpStatus=null;
    Map<String,Object> map=new HashMap<>();
    private ArticleService articleService;
    ArticleRestController(ArticleService articleService){
        this.articleService=articleService;
    }
    @GetMapping
   public ResponseEntity<Map<String,Object>> findAllArtilce(){
        List<Article> articles=articleService.findAllArtilce();
        try {
            if (!articles.isEmpty()) {
                httpStatus=HttpStatus.OK;
                map.put("code","200");
                map.put("message","Successfull");
                map.put("status",true);
                map.put("data",articles);
            }else{
                httpStatus=HttpStatus.NOT_FOUND;
                map.put("code","400");
                map.put("message","Fail");
                map.put("status",false);
                map.put("data",articles);
            }
        }catch (Exception e){
            e.printStackTrace();
            map.put("code","500");
            map.put("message","Something is broken hard");
            map.put("status",false);
            httpStatus=HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return  new ResponseEntity<Map<String, Object>>(map,httpStatus=HttpStatus.OK);
    }
    //find Article by artilce hash
    @GetMapping("/uudi/{userHasd}")
    public ResponseEntity<Map<String,Object>>findArticleOne(@PathVariable String userHash){
       Article article=articleService.findArticleOne(userHash);
       try{
            if (article!=null){
                httpStatus=HttpStatus.OK;
                map.put("","200");
                map.put("","Success!");
                map.put("stats",true);
                map.put("data",article);

            }else{
                httpStatus=HttpStatus.OK;
                map.put("code","400");
                map.put("massege","Success!");
                map.put("stats",true);
                httpStatus=HttpStatus.NOT_FOUND;
            }
       }catch (Exception e){
           e.printStackTrace();
           map.put("code","500");
           map.put("message","Somtething is broken");
           map.put("status",false);
           httpStatus=HttpStatus.INTERNAL_SERVER_ERROR;
       }
       return new ResponseEntity<Map<String, Object>>(map,httpStatus.OK);
    }
    //Article​ save
    @PostMapping("/save")
    public ResponseEntity<Map<String,Object>> save(@RequestBody Article article){
        try{
            if(!articleService.save(article)){
                httpStatus=HttpStatus.OK;
                map.put("code","200");
                map.put("massage","Success!");
                map.put("stats",true);
            }else{
                httpStatus=HttpStatus.OK;
                map.put("","400");
                map.put("","Success!");
                map.put("stats",true);
                httpStatus=HttpStatus.NOT_FOUND;
            }
        }catch (Exception e){
            e.printStackTrace();
            map.put("code","500");
            map.put("message","Somtething is broken");
            map.put("status",false);
            httpStatus=HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<Map<String, Object>>(map,httpStatus.OK);
    }
    @PutMapping("/delete/{userHash}")
    public ResponseEntity<Map<String,Object>> delete(String userHash){
        Boolean delete=articleService.delete(userHash);
        try{

        }catch (Exception e){

        }
        return new ResponseEntity<Map<String, Object>>(map,httpStatus.OK);
    }


}
