package org.kshrd.saren.spring_homeword_article.models;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class Article {
    private int id;
    private Category category;
    @NotBlank
    @Size(min=5, max=50)
    private String title;
    @NotBlank
    @Size(min=10, max=1000)
    private String desc;
    private String thumbnail;
    private String articleHash;
    private String importDate;
    @NotBlank
    private String author;
    public Article(){
        super();
    }
    public Article(int id, Category category, String title, String desc, String thumbnail, String articleHash, String importDate, String author) {
        this.id = id;
        this.category = category;
        this.title = title;
        this.desc = desc;
        this.thumbnail = thumbnail;
        this.articleHash = articleHash;
        this.importDate = importDate;
        this.author = author;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getArticleHash() {
        return articleHash;
    }

    public void setArticleHash(String articleHash) {
        this.articleHash = articleHash;
    }

    public String getImportDate() {
        return importDate;
    }

    public void setImportDate(String importDate) {
        this.importDate = importDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Article{");
        sb.append("id=").append(id);
        sb.append(", category=").append(category);
        sb.append(", title='").append(title).append('\'');
        sb.append(", desc='").append(desc).append('\'');
        sb.append(", thumbnail='").append(thumbnail).append('\'');
        sb.append(", articleHash='").append(articleHash).append('\'');
        sb.append(", importDate='").append(importDate).append('\'');
        sb.append(", author='").append(author).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
