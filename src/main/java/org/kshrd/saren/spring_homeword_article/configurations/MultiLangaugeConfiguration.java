package org.kshrd.saren.spring_homeword_article.configurations;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Locale;

@Configuration
public class MultiLangaugeConfiguration extends WebMvcConfigurerAdapter {
   @Bean
    public LocaleResolver localeResolver(){
        SessionLocaleResolver slr=new SessionLocaleResolver();
        //slr.setDefaultLocale(Locale.US);
        Locale defaultLocale=new Locale("kh");
        slr.setDefaultLocale(defaultLocale);
        return slr;
    }
    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor(){
        LocaleChangeInterceptor lci=new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
       registry.addInterceptor(this.localeChangeInterceptor());
    }
    @Bean
    public MessageSource messageSource(){
        ResourceBundleMessageSource rbs=new ResourceBundleMessageSource();
        rbs.setBasename("languages/messages");
        rbs.setCacheSeconds(0);
        rbs.setUseCodeAsDefaultMessage(true);
        rbs.setDefaultEncoding("UTF-8");
        return rbs;
    }
}
