package org.kshrd.saren.spring_homeword_article.services;

import org.kshrd.saren.spring_homeword_article.models.Category;

import java.util.List;

public interface CategoryService {
    public List<Category>findCategory();
    public Category finOne(int id);
    public boolean save(Category category);
    public boolean update(Category category);
    public boolean delete(int id);
}
