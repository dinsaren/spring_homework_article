package org.kshrd.saren.spring_homeword_article;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringHomewordArticleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringHomewordArticleApplication.class, args);
	}
}
