package org.kshrd.saren.spring_homeword_article.controllers;

import org.kshrd.saren.spring_homeword_article.models.Article;
import org.kshrd.saren.spring_homeword_article.models.Category;
import org.kshrd.saren.spring_homeword_article.services.ArticleService;
import org.kshrd.saren.spring_homeword_article.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
public class ArticleController {
    Category category;
    private ArticleService articleService;
    //Inject ArticleService
    @Autowired
    public ArticleController (ArticleService articleService){
        this.articleService=articleService;
    }
    private CategoryService categoryService;
    //Inject CategoryService
    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    //method get all Article
    @GetMapping("admin/list_article")
    //@ResponseBody
    public  String getAllArticle(ModelMap m){
        m.addAttribute("articels",articleService.findAllArtilce());
        return "admin/article/list";
    }

    // Add Article to form
    @GetMapping("admin/add_article")
    public String addToFrom(Article article, ModelMap m){
        m.addAttribute("categories",categoryService.findCategory());

        m.addAttribute("article",article);
        return "admin/article/add";
    }
    //Save Article
    @PostMapping("admin/save_article")
    public String save(@Valid @ModelAttribute  Article article,  BindingResult result, ModelMap m,MultipartFile file){
        if(result.hasErrors()){
            m.addAttribute("categories",categoryService.findCategory());
            m.addAttribute("article",article);
            return "admin/article/add";
        }
        String userHash=UUID.randomUUID().toString();
        String serverPath="C://Users/SR_NEW/Desktop/Image/";
        String fileName= UUID.randomUUID()+"."+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
        if(!file.isEmpty()){
            try {
                Files.copy(file.getInputStream(), Paths.get(serverPath,fileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (file.isEmpty()){
            fileName="none.png";
        }
        article.setImportDate(new Date().toString());
        article.setCategory(categoryService.finOne(article.getCategory().getId()));
        article.setArticleHash(userHash);
        article.setThumbnail("/article/images/"+fileName);
        System.out.println(article);
        articleService.save(article);
        return "redirect:/admin/list_article";
    }

    //add article one to form edite
    @GetMapping("admin/update_article/{article_hash}")
    public String addToForm(@PathVariable String article_hash , ModelMap model){
        Article article=articleService.findArticleOne(article_hash);
        model.addAttribute("categories",categoryService.findCategory());
        model.addAttribute("article",article);
        return "admin/article/update";
    }
    //Article Update Commit
    @PostMapping("admin/update_article/submit")
    public String update(@Valid @ModelAttribute  Article article,  BindingResult result, MultipartFile file, ModelMap m){
        if(result.hasErrors()){
            m.addAttribute("categories",categoryService.findCategory());
            m.addAttribute("article",article);
            return "admin/article/update";
        }
        String serverPath="C://Users/SR_NEW/Desktop/Image/";
        String fileName= UUID.randomUUID()+"."+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")+1);
        if(!file.isEmpty()){
            try {
                Files.copy(file.getInputStream(), Paths.get(serverPath,fileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
            article.setThumbnail("/article/images/"+fileName);
        }else{
            article.setThumbnail(article.getThumbnail());
        }
        article.setCategory(categoryService.finOne(article.getCategory().getId()));
        System.out.println(article.getArticleHash());
        System.out.println(article.getThumbnail());
        articleService.update(article);
        return "redirect:/admin/list_article";
    }
    //ResponeBody API
    @GetMapping("/all")
    public List<Article>all(){
        return articleService.findAllArtilce();
    }
    //find article one
    @GetMapping("admin/find_article/{article_hash}")
    public String findArticleOne(@PathVariable String article_hash,ModelMap model){
        Article article=articleService.findArticleOne(article_hash);
        model.addAttribute("article",article);
        System.out.println(article);
        return "admin/article/view";
    }
    //delete article
    @GetMapping("admin/delete_article/{article_hash}")
    public String deleteArticel(@PathVariable String article_hash){
        articleService.delete(article_hash);
        return "redirect:/admin/list_article";
    }
}
