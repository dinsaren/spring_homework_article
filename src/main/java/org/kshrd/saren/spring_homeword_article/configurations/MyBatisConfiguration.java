package org.kshrd.saren.spring_homeword_article.configurations;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

@Configuration
@MapperScan("org.kshrd.saren.spring_homeword_article.reposities")
public class MyBatisConfiguration {

    private DataSource dataSource;
    MyBatisConfiguration(@Qualifier("dataSource") DataSource dataSource1){
        this.dataSource=dataSource1;
    }
    //    @Autowired
//    public MyBatisConfiguration(DataSource dataSource) {
//        this.dataSource = dataSource;
//    }



    @Bean
    public DataSourceTransactionManager dataSourceTransactionManager(){
        return new DataSourceTransactionManager(dataSource);
    }
    @Bean
    public SqlSessionFactoryBean sqlSessionFactoryBean() {
        SqlSessionFactoryBean sqlf=new SqlSessionFactoryBean();
        sqlf.setDataSource(dataSource);
        return sqlf;
    }


}
