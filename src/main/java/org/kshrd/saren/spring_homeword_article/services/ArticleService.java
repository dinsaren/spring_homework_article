package org.kshrd.saren.spring_homeword_article.services;

import org.kshrd.saren.spring_homeword_article.models.Article;


import java.util.List;


public interface ArticleService {
    List<Article>findAllArtilce();
    Boolean save(Article article);
    public Article findArticleOne(String articleHash);
    boolean delete(String articleHash);
    public boolean update(Article article);


}
